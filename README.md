# Платформенные сервисы

Репозиторий содержит инструкции (IaC) для установки платформенных сервисов в K8s кластер. 

## Настройка Gitlab CI

Для того чтобы использовать пайплайны Gitlab CI, потребуется настроить интеграцию Gitlab CI и Kubernetes согласно инструкции https://docs.gitlab.com/ee/user/project/clusters/add_remove_clusters.html#add-existing-cluster

Шаги из инструкции перенесены в скрипт-визард

    ./gitlab-setup-helper.sh
    
после выполнения которого останется только подставить значения в интерфейс добавления нового кластера в Gitlab.

Устанавливаемые сервисы:
- Nginx Ingress Controller
- Cert Manager + Acme Cert Manager Issuer
- Prometheus Operator (Prometheus + Grafana + Alertmanager)
