#!/usr/bin/env bash
set -e

function _confirm() {
  while [[ true ]]; do
    read -p "" agree
    case "$agree" in
      y|yes)
        echo -e "\e[1mok!\e[0m"
        return
        ;;
      n|no)
        echo -e "\e[1mok!\e[0m"
        exit 11
        ;;
      *)
        printf "\e[1mYou should type in 'yes' or 'no':\e[0m "
        ;;
    esac
  done
}

echo -e "\e[1mAvailable kubectl contexts: \e[0m"
kubectl config get-contexts

printf "\n\e[1mDo you want to use current context? [y/n]\e[0m "
_confirm
echo ""

echo -e "\e[1mCluster API: \e[0m"
kubectl cluster-info | grep 'Kubernetes master' | awk '/http/ {print $NF}'
echo ""

echo -e "\e[1mCertificate: \e[0m"
kubectl get secret $(kubectl get secrets | grep default-token | awk '{print $1}') -o jsonpath="{['data']['ca\.crt']}" | base64 --decode
echo ""

printf "\e[1mNext, I'm going to create ServiceAccount for Gitlab. Do you approve this? [y/n]\e[0m "
_confirm

echo -e "\e[1mCreating ServiceAccount ... \e[0m"
cat <<EOF | kubectl apply -f -
apiVersion: v1
kind: ServiceAccount
metadata:
  name: gitlab
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: gitlab-admin
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
  - kind: ServiceAccount
    name: gitlab
    namespace: kube-system
EOF
echo ""

echo -e "\e[1mAccess token: \e[0m"
kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep gitlab | awk '{print $1}')
echo ""

echo -e "\e[1mDone! You can use this info to integrate Gitlab with K8s cluster.\e[0m"
